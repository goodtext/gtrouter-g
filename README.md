# GTRouter
![输入图片说明](Src/logo.png)
#### 介绍
GTRoute - 简单、快速灵活的PHP路由。便于快速创建和管理匹配URL。 静态路由、正则路由 -- 基于动态 PCRE 的路由模式。当定义的路由规则与当前URL匹配时，将执行附加的路由处理函数。处理一条匹配路由。当没有找到匹配的路由时，将输出404。

## 使用说明

适用环境：Apache 2.4及以上；PHP 7.4或更新版本；开启 URL Rewriting

.htaccess（Apache）：


```
RewriteEngine On
RewriteCond %{REQUEST_FILENAME} !-f
RewriteCond %{REQUEST_FILENAME} !-d
RewriteRule ^(.*)$ index.php?$1 [QSA,L]
```


.htaccess（Nginx）：


```
rewrite ^/(.*)/$ /$1 redirect;
if (!-e $request_filename){
    rewrite ^(.*)$ /index.php break;
}

```



### 快速开始用法

创建一个Router实例，在上面定义一些路由，然后运行它。


```
// 自动加载
require __DIR__ . '/vendor/autoload.php';

// 创建实例
$router = new \GoodText\App\Router();

// 定义路由
// ...

// 开始执行
$router->dispatch();
```


```
define('SYS_PATH', __DIR__ . '/Src/');
require SYS_PATH.'Router.php';
use \GoodText\App\Router;

$router = new Router();
$router->addRoute('GET', '/hello', function() {
    echo 'hello';
});
$router->dispatch();
```
### 添加路由

使用以下方法添加路由
`$router->addRoute('GET|POST', 'pattern', function() { … });`
当路由与当前URL（$_SERVER['REQUEST_URI']）匹配时，将执行附加的路由处理函数。路由处理函数必须是可调用的。处理一条匹配路由。当没有找到匹配的路由时，将输出404。

### 路由模式
路由模式可以是静态路由或正则路由： 静态路由不包含动态参数的最简单的路由，直接与当前URL的路径部分进行比较，必须完全匹配。 正则路由包含可以随请求而变化的动态部分，使用与Perl兼容的正则表达式（PCRE）进行定义

#### 静态路由

```
$router->addRoute('GET', '/home', function() {
    echo 'https://www.goodtext.cn/home Page Contents';
});
$router->addRoute('GET', '/index', function() {
    echo 'https://www.goodtext.cn/index Page Contents';
});
$router->addRoute('GET', '/test', function() {
    echo 'https://www.goodtext.cn/test Page Contents';
});
$router->addRoute('GET', '/hello', function() {
    echo 'https://www.goodtext.cn/hello Page Contents';
});
```

#### 正则路由

常用的基于PCRE的子模式
- \d+ 一个或多个数字（0-9）
- \w+ 一个或多个文字字符（az 0-9 _）
- [a-z0-9_-]+ 一个或多个文字字符（az 0-9 _）和破折号（-）
- .* 任何字符（包括/），零个或多个
- [^/]+ /以外的任何字符，一个或多个
```
$router->addRoute('GET', '/news/(\d+)', function($id) {
    echo 'news ' . $id;
});

$router->addRoute('GET', '/hello/(\w+)', function($name) {
    echo 'Hello ' . htmlentities($name);
});

$router->addRoute('GET', '/type/(\d+)/article/(\d+)', function($typeId, $articleId) {
    echo 'type #' . $typeId . ', article #' . $articleId;
});
```
博客URL

```
$router->addRoute('GET', '/blog(/\d{4}(/\d{2}(/\d{2}(/[a-z0-9_-]+)?)?)?)?', function($year = null, $month = null, $day = null, $slug = null) {
    // ...
});
```
建议始终定义连续的可选参数。

### 匹配控制器
当请求与指定的路由URL匹配时，将执行该类上的方法。定义的路由参数将传递给类方法。通过setNamespace()设置命名空间。
```
$router->setNamespace('\App\Controllers');
$router->addRoute('GET', '/users/(\d+)', 'User@show');

```

//直接匹配

```
$router->setNamespace('\App\Controllers');
$router->addRoute('GET', '/', 'user@index');

```

## 运行示例

```
//
//POST提交
$router->addRoute('GET|POST', '/hello123/', function() {
    echo 'What is your name?';
    echo '<form method=post><input type=text name=username><input type=submit></form>';
    echo 'Hello ',$_POST['username'],', how are you?';
});

$router->setControllerPath('/c'); // 设置控制器类的路径
$router->setNamespace('\App\Controllers'); // 通过setNamespace（）设置路由上使用的命名空间
$router->addRoute('GET|POST', '/login', 'index@login');
$router->addRoute('GET|POST', '/users(/\d+)?', 'index@users');
$router->addRoute('GET|POST', '/demo_api', 'index@demo_api');

$router->dispatch();
```
示例2

```
<?php
require __DIR__ . '/Src/Router.php';

use \GoodText\App\Router;

$router = new Router();

$router->addRoute('GET', '/', function () {

    echo '<h1>Route</h1><p>[GET]:<p><ul>';

    echo '<li><a href="/home">/home</a></li>';
    echo '<li><a href="/info/only">/info/only</a></li>';
    echo '<li><a href="/news/566">/news/566</a></li>';
    echo '<li><a href="/news/45/page/22">/news/45/page/22</a></li>';

    echo '<li><a href="/data/search">/data/search</a></li>';
    echo '<li><a href="/data/search/dataset">/data/search/dataset</a></li>';
    echo '<li><a href="/url/item/list/1-1">/url/item/list/1-1</a></li>';
    echo '<li><a href="/date/2023/09/28">/date/2023/09/28</a></li>';

    echo '<li><a href="/api">/api</a></li>';
    echo '<li><a href="/admin/page">/admin/page</a></li>';

    echo '<li><a href="/login">/login</a></li>';
    echo '<li><a href="/demo_api">/demo_api</a></li>';
    echo '<li><a href="/users/56">/users/56</a></li>';
    echo '</ul>';
    echo '<p>[POST]:<p><ul>';

    echo '<li><a href="/hello">/hello</a></li>';
    echo '</li>';

    echo '</ul>';
});

$router->addRoute('GET', '/home', function () {
    echo 'https://www.goodtext.cn/home Page Contents';
});
//POST提交
$router->addRoute('GET|POST', '/hello', function () {
    echo 'What is your name?';
    echo '<form method=post><input type=text name=username> <input type=submit></form>';
    echo 'Hello ', @$_POST['username'], ', how are you?';
});

$router->addRoute('GET', '/info/(\w+)', function ($name) {
    echo 'Hello ' . htmlentities($name);
});

$router->addRoute('GET', '/news/(\d+)', function ($id) {
    echo 'news:' . $id;
});

$router->addRoute('GET', '/news/(\d+)/page/(\d+)', function ($id, $pageid) {
    echo 'news:' . $id . ' page:' . $pageid;
});

$router->addRoute('GET', '/data/([a-z0-9_-]+)', function ($string) {
    echo $string;
});

$router->addRoute('GET', '/data/([a-z0-9_-]+)/([a-z0-9_-]+)', function ($string, $string2) {
    echo $string . ' + ' . $string;
});

$router->addRoute('GET', '/url/(.*)', function ($url) {
    echo 'url ' . htmlentities($url);
});

$router->addRoute('GET', '/date(/\d{4}(/\d{2}(/\d{2})?)?)?', function ($year = null, $month = null, $day = null) {
    echo 'date ' . $year . ' ' . $month . ' ' . $day;
});

$router->addRoute('GET', '/api(/.*)?', function () {
    header('Content-Type:application/json; charset=utf-8');
    $jsonArray                = array();
    $jsonArray['status']      = "ok";
    $jsonArray['status_text'] = "data";

    echo json_encode($jsonArray);
});

$router->addRoute('GET|POST', '/admin/.*', function () {
    if (!isset($_SESSION['user'])) {
        header('location: /user/login');
        exit();
    }
});
$router->dispatch();

```


