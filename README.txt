# GTRouter
![输入图片说明](Src/logo.png)
#### 介绍
GTRouter轻量级的PHP路由器 
当路由与当前URL匹配时，将执行附加的路由处理函数。路由处理函数必须是可调用的。只有第一个匹配的路由会被处理。当没有找到匹配的路由时，将执行 404 处理程序。
支持GET，POST，PUT，DELET，OPTIONS和PATCH Request Head 方法。静态路由模式、动态路由模式 -- 基于动态 PCRE 的路由模式。

适用环境：Apache 2.4及以上；PHP 7.4或更新版本；开启 URL Rewriting

使用以下方法添加路由

`$router->addRoute('GET|POST', 'pattern', function() { … });`

快速开始

```
define('SYS_PATH', __DIR__ . '/src/');
require SYS_PATH.'Router.php';
use \GoodText\App\Router;

$router = new Router();
$router->addRoute('GET', '/hello', function() {
    echo 'hello';
});
$router->dispatch();
```

路由模式
路由模式可以是静态或动态的： 静态路由模式不包含动态部分，直接与当前url的路径部分进行比较，并且必须完全匹配。 动态路由模式包含可以随请求而变化的动态部分。不同的部分称为子模式，并使用与Perl兼容的正则表达式（PCRE）或使用占位符进行定义

静态路由模式：

```
$router->addRoute('GET', '/home', function() {
    echo 'home Page Contents';
});
$router->addRoute('GET', '/index', function() {
    echo 'index Page Contents';
});
$router->addRoute('GET', '/test', function() {
    echo 'test Page Contents';
});
$router->addRoute('GET', '/hello', function() {
    echo 'hello Page Contents';
});
```

动态路由模式中常用的基于PCRE的子模式为：


- \d+ 一个或多个数字（0-9）
- \w+ 一个或多个文字字符（az 0-9 _）
- [a-z0-9_-]+ 一个或多个文字字符（az 0-9 _）和破折号（-）
- .* 任何字符（包括/），零个或多个
- [^/]+ /以外的任何字符，一个或多个

动态路由模式：

```
$router->addRoute('GET', '/news/(\d+)', function($id) {
    echo 'news ' . $id;
});

$router->addRoute('GET', '/hello/(\w+)', function($name) {
    echo 'Hello ' . htmlentities($name);
});

$router->addRoute('GET', '/type/(\d+)/article/(\d+)', function($typeId, $articleId) {
    echo 'type #' . $typeId . ', article #' . $articleId;
});
```


匹配控制器方法
当请求匹配指定的路由URI时，将执行该类上的方法。定义的路由参数将传递给class方法。通过setNamespace()设置命名空间。



```
$router->setNamespace('\App\Controllers');
$router->addRoute('GET', '/users/(\d+)', 'User@showProfile');

```

//直接匹配

```
$router->setNamespace('\App\Controllers');
$router->addRoute('GET', '/', 'user@index');

```

实例：


```
//
//POST提交
$router->addRoute('GET|POST', '/hello123/', function() {
	echo 'What is your name?';
    echo '<form method=post><input type=text name=username><input type=submit></form>';

    echo 'Hello ',$_POST['username'],', how are you?';
});

//模板使用
$router->addRoute('GET', '/login', function() {
        $age = array("Bill"=>"", "Steve"=>"56", "Mark"=>"31");
        Application::view(APP_PATH.VIEWS_PATH.'index', $age);
    });

```



#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
