<?php
namespace App\Controllers;
class My_indexController
{

    public function my_loginAction()
    {
        echo '登录页';
    }

    public function my_usersAction($id = null)
    {
        if ($id) {
            echo '用户ID：' . $id;
        } else {
            echo '没有用户ID';
        }
    }
    public function My_demo_apiAction()
    {
        echo 'demo_api';
    }

    public function My_docsAction()
    {
        $num       = mt_rand(0, 3);
        $view_data = array('bg_num' => $num);
        Application::view(APP_PATH . VIEWS_PATH . 'docs', $view_data);
    }

}
