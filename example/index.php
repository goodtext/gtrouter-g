<?php
// ini_set('display_errors', 'on');
// error_reporting(0); 禁用错误报告
// error_reporting(E_ERROR | E_WARNING | E_PARSE); 报告运行时错误
// error_reporting(E_ALL); 报告所有错误
require __DIR__ . '/../Src/Router.php';

use \GoodText\App\Router;

$router = new Router();

$router->addRoute('GET', '/', function () {

    echo '<h1>Route</h1><ul>';

    echo '<li><a href="/example/home">/home</a></li>';
    echo '<li><a href="/example/info/only">/info/only</a></li>';
    echo '<li><a href="/example/news/566">/news/566</a></li>';
    echo '<li><a href="/example/news/45/page/22">/news/45/page/22</a></li>';

    echo '<li><a href="/example/data/search">/data/search</a></li>';
    echo '<li><a href="/example/data/search/dataset">/data/search/dataset</a></li>';
    echo '<li><a href="/example/url/item/list/1-1">/url/item/list/1-1</a></li>';
    echo '<li><a href="/example/date/2023/09/28">/date/2023/09/28</a></li>';

    echo '<li><a href="/example/api">/api</a></li>';
    echo '<li><a href="/example/admin/page">/admin/page</a></li>';

    echo '<li><a href="/example/login">/login</a></li>';
    echo '<li><a href="/example/demo_api">/demo_api</a></li>';
    echo '<li><a href="/example/users/56">/users/56</a></li>';
    echo '<li><a href="/example/hello">/hello</a></li>';
    echo '</li>';

    echo '</ul>';
});

$router->addRoute('GET', '/home', function () {
    echo 'https://www.goodtext.cn/home Page Contents';
});
//POST提交
$router->addRoute('GET|POST', '/hello', function () {
    echo 'What is your name?';
    echo '<form method=post><input type=text name=username> <input type=submit></form>';
    echo 'Hello ', @$_POST['username'], ', how are you?';
});

$router->addRoute('GET', '/info/(\w+)', function ($name) {
    echo 'Hello ' . htmlentities($name);
});

$router->addRoute('GET', '/news/(\d+)', function ($id) {
    echo 'news:' . $id;
});

$router->addRoute('GET', '/news/(\d+)/page/(\d+)', function ($id, $pageid) {
    echo 'news:' . $id . ' page:' . $pageid;
});

$router->addRoute('GET', '/data/([a-z0-9_-]+)', function ($string) {
    echo $string;
});

$router->addRoute('GET', '/data/([a-z0-9_-]+)/([a-z0-9_-]+)', function ($string, $string2) {
    echo $string . ' + ' . $string;
});

$router->addRoute('GET', '/url/(.*)', function ($url) {
    echo 'url ' . htmlentities($url);
});

$router->addRoute('GET', '/date(/\d{4}(/\d{2}(/\d{2})?)?)?', function ($year = null, $month = null, $day = null) {
    echo 'date ' . $year . ' ' . $month . ' ' . $day;
});

$router->addRoute('GET', '/api(/.*)?', function () {
    header('Content-Type:application/json; charset=utf-8');
    $jsonArray                = array();
    $jsonArray['status']      = "ok";
    $jsonArray['status_text'] = "data";

    echo json_encode($jsonArray);
});

$router->addRoute('GET|POST', '/admin/.*', function () {
    if (!isset($_SESSION['user'])) {
        header('location: /example/login');
        exit();
    }
});

$router->setControllerPath('/c'); // 设置控制器类的路径
$router->setNamespace('\App\Controllers'); // 通过setNamespace（）设置路由上使用的命名空间
$router->addRoute('GET|POST', '/login', 'index@login');
$router->addRoute('GET|POST', '/users(/\d+)?', 'index@users');
$router->addRoute('GET|POST', '/demo_api', 'index@demo_api');


$router->dispatch();